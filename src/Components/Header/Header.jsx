import styles from "./Header.module.scss";

import React from "react";
import PropTypes from "prop-types";

import { ReactComponent as Cart } from "../../assets/svg/cart.svg";
import { ReactComponent as Wish } from "../../assets/svg/wish.svg";

const Header = ({ wish, cart }) => {
    return (
        <section className={styles.header}>
            <div className={styles.container}>
                <div className={styles.buttons}>
                    <button>
                        <Cart />
                        <span>{cart ? cart.length : 0}</span>
                    </button>
                    <button>
                        <Wish />
                        <span>{wish ? wish.length : 0}</span>
                    </button>
                </div>
            </div>
        </section>
    );
};

Header.propTypes = {
  wish: PropTypes.array,
  cart: PropTypes.array
};

Header.defaultProps = {
  wish: [],
  cart: []
};

export default Header;
