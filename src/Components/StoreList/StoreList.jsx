import StoreItem from "../StoreItem/StoreItem";
import styles from "./StoreList.module.scss";
import PropTypes from "prop-types"



const StoreList = ({ store, setCart, setWish }) => {

    return (
        <section className={styles.store}>
            <div className={styles.container}>
                {store.map((item) => {
                    // console.log(item)
                    return (
                        <StoreItem key={item.id} item={item} setCart={setCart} setWish={setWish} />
                    )
                })}
            </div>
        </section>
    )
}

StoreList.propTypes = {
    store: PropTypes.array,
    cart: PropTypes.array,
    favor: PropTypes.array
}

StoreList.defaultProps = {
    store: [],
    cart: [],
    favor: []
}

export default StoreList