import styles from "./StoreItem.module.scss";

import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";

import {
  saveToLocalStorage,
  getFromLocalStorage,
} from "../../utils/localStorage/localStorage";

import { ReactComponent as Star } from "../../assets/svg/star.svg";
import { ReactComponent as StarFilled } from "../../assets/svg/star_filled.svg";
import Modal from "../Modal/Modal";

const StoreItem = ({ item, setCart, setWish }) => {
  const [isFavor, setIsFavor] = useState(false);
  const [isCart, setIsCart] = useState(false);
  const [isModal, setIsModal] = useState(false)

  const { id, name, price, url, width, height, article, color } = item;

  useEffect(() => {
    const wishLS = getFromLocalStorage("wishList");
    const cartLS = getFromLocalStorage("cartList");

    const findWish = wishLS ? Boolean(wishLS.find((el) => el.id === id)) : false;
    const findCart = cartLS ? Boolean(cartLS.find((el) => el.id === id)) : false;

    if(findWish) {
        setIsFavor(true)
    } else {
        setIsFavor(false)
    }

    if(findCart) {
        setIsCart(true)
    } else {
        setIsCart(false)
    }
    
  }, []);

  const toggleCart = () => {
    setCart((prev) => {
      const fantomState = [...prev];
      const index = fantomState.find((el) => el.id === id);
      setIsModal(true)

      if (index) {
        const newState = fantomState.filter((el) => el.id !== id);
        saveToLocalStorage("cartList", newState);
        setIsCart(false);
        return newState;
      } else {
        const newState = [item, ...prev];
        saveToLocalStorage("cartList", newState);
        setIsCart(true);
        return newState;
      }
    });
  };

  const toggleFavor = () => {
    setWish((prev) => {
      const fantomState = [...prev];
      const index = fantomState.find((el) => el.id === id);

      if (index) {
        const newState = fantomState.filter((el) => el.id !== id);
        saveToLocalStorage("wishList", newState);
        setIsFavor(false);
        return newState;
      } else {
        const newState = [item, ...prev];
        saveToLocalStorage("wishList", newState);
        setIsFavor(true);
        return newState;
      }
    });
  };

  const close = () => {
    setIsModal(false)
  }

  return (
    <>
      <div className={styles.card}>
        <img src={url} alt="#" width={width} height={height} />
        <p className={styles.article}>{article}</p>
        <h2 className={styles.title}>{name}</h2>
        <div className={styles.description}>
          <div style={{ backgroundColor: color }}></div>
          <p>{price}</p>
        </div>
        <div className={styles.buttons}>
          <Button
            classes={styles.favBtn}
            text={isFavor ? <StarFilled /> : <Star />}
            onClick={toggleFavor}
          />
          <Button
            classes={styles.btn}
            text={isCart ? "Remove from the cart" : "Add to cart"}
            onClick={toggleCart}
          />
        </div>
      </div>
      { isModal && (
        <Modal 
          text={isCart ? "Товар додано до кошика" : "Товар вилучено з кошика"}
          closeModal={close}
        />
      ) }
    </>
  );
};

StoreItem.propTypes = {
  item: PropTypes.object,
  setCart: PropTypes.func,
  setWish: PropTypes.func,
};

StoreItem.defaultProps = {
  item: {},
  setCart: () => {},
  setWish: () => {},
};

export default StoreItem;
