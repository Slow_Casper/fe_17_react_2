import React, { useEffect, useState } from "react";
import axios from "axios";
import StoreList from "./Components/StoreList/StoreList";
import {
    getFromLocalStorage,
    saveToLocalStorage,
} from "./utils/localStorage/localStorage";

import "./index.css";
import Header from "./Components/Header/Header";

function App() {
    const [store, setStore] = useState([]);
    const [wish, setWish] = useState([]);
    const [cart, setCart] = useState([]);

    const getStore = async () => {
        const { data } = await axios("./store/store.json");
        setStore(data);

        if (!localStorage.cartList && !localStorage.wishList) {
            saveToLocalStorage("wishList", []);
            saveToLocalStorage("cartList", []);
        }
        setWish(getFromLocalStorage("wishList"));
        setCart(getFromLocalStorage("cartList"));
    };

    useEffect(() => {
        getStore();
    }, []);

    return (
        <div className="App">
            <Header wish={wish} cart={cart} />
            <StoreList store={store} setWish={setWish} setCart={setCart} />
        </div>
    );
}

export default App;
